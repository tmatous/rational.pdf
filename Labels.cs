﻿//  Copyright 2014 Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Rational.PDF.Labels
{

    public class LabelPageSettings : PageSettings
    {

        public LabelPageSettings() : base() { }
        public LabelPageSettings(ePageSize pPresetSize) : base(pPresetSize) { }


        public Int32 Rows { get; set; }
        public Int32 Cols { get; set; }
        public Single LabelHeight { get; set; }
        public Single LabelWidth { get; set; }
    }

    public class LabelStyle : ParagraphStyle
    {
        public eVerticalAlign VerticalAlign { get; set; }
        public eRotation Rotation { get; set; }
    }


    public class LabelGenerator
    {

        public Single BorderWidth { get; set; }

        public void GenerateLabelsFile(System.IO.FileInfo pOutputFile, LabelPageSettings pSettings, IEnumerable<string> pLabelData, LabelStyle pLabelStyle)
        {
            Func<object, PdfPCell> renderer = pData =>
            {
                var data = "";
                if (pData != null) data = pData.ToString();
                var pgh = new Paragraph();
                Utilities.SetParagraphStyle(pgh, pLabelStyle);
                pgh.Add(data);
                var cell = new PdfPCell(pgh);
                cell.VerticalAlignment = Utilities.TranslateVerticalAlign(pLabelStyle.VerticalAlign);
                cell.HorizontalAlignment = Utilities.TranslateHorizontalAlign(pLabelStyle.HorizontalAlign);
                cell.Rotation = Utilities.TranslateRotation(pLabelStyle.Rotation);

                return cell;
            };

            GenerateLabelsFileBase(pOutputFile, pSettings, pLabelData.ToList(), renderer);
        }


        public void GenerateLabelsFile(System.IO.FileInfo pOutputFile, LabelPageSettings pSettings, IEnumerable<object> pLabelData, string pLabelTemplate)
        {
            Func<object, PdfPCell> renderer = null;
            var sampleData = (from d in pLabelData where d != null select d).FirstOrDefault();
            if (XmlToPdf.CheckLayout(pLabelTemplate, sampleData))
            {
                renderer = pData =>
                {
                    if (pData != null)
                    {
                        return XmlToPdf.RenderLabelTemplate(pLabelTemplate, pData);
                    }
                    else
                    {
                        return new PdfPCell(new Phrase(" "));
                    }
                };
            }
            else
            {
                //problem with template xml, just use it as a string template
                var labelStyle = new LabelStyle
                {
                    FontFamily = "Arial",
                    FontSize = 10,
                    FontColor = System.Drawing.Color.Black,
                    HorizontalAlign = eHorizontalAlign.Left,
                    VerticalAlign = eVerticalAlign.Middle,
                    Rotation = eRotation.None
                };

                renderer = pData =>
                {
                    var data = XmlToPdf.PopulateTemplateData(pLabelTemplate, pData);
                    var pgh = new Paragraph();
                    Utilities.SetParagraphStyle(pgh, labelStyle);
                    pgh.Add(data);
                    var cell = new PdfPCell(pgh);
                    cell.VerticalAlignment = Utilities.TranslateVerticalAlign(labelStyle.VerticalAlign);
                    cell.HorizontalAlignment = Utilities.TranslateHorizontalAlign(labelStyle.HorizontalAlign);
                    cell.Rotation = Utilities.TranslateRotation(labelStyle.Rotation);
                    cell.PaddingLeft = 5;
                    return cell;
                };
            }

            GenerateLabelsFileBase(pOutputFile, pSettings, pLabelData.ToList(), renderer);
        }

        private void GenerateLabelsFileBase(System.IO.FileInfo pOutputFile, LabelPageSettings pSettings, IList pLabelData, Func<object, PdfPCell> pLabelCellRenderer)
        {
            System.IO.FileStream fs = null;
            try
            {
                fs = new System.IO.FileStream(pOutputFile.FullName, System.IO.FileMode.Create);
                GenerateLabelsBase(fs, pSettings, pLabelData, pLabelCellRenderer);
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }
 

        private void GenerateLabelsBase(System.IO.Stream pOutputStream, LabelPageSettings pSettings, IList pLabelData, Func<object, PdfPCell> pLabelCellRenderer)
        {
            if (!pSettings.Height.HasValue)
                throw new Exception("Page height is required");

            var availWidth = pSettings.Width - pSettings.MarginLeft - pSettings.MarginRight;
            float colMargin = 0;
            int tableCols = pSettings.Cols + (pSettings.Cols - 1);
            if (pSettings.Cols > 1)
                colMargin = (availWidth - (pSettings.Cols * pSettings.LabelWidth)) / (pSettings.Cols - 1);
            if (colMargin < 0)
                throw new Exception("Specified labels will not fit in page width");
            var labelWidthPct = pSettings.LabelWidth / availWidth;
            var colMarginPct = colMargin / availWidth;
            var colWidths = new List<float>();
            for (int colIdx = 0; colIdx <= pSettings.Cols - 1; colIdx++)
            {
                if (colIdx > 0)
                    colWidths.Add(colMarginPct);
                colWidths.Add(labelWidthPct);
            }

            var availHeight = pSettings.Height.Value - pSettings.MarginTop - pSettings.MarginBottom;
            float rowMargin = 0;
            if (pSettings.Rows > 1)
                rowMargin = (availHeight - (pSettings.Rows * pSettings.LabelHeight)) / (pSettings.Rows - 1);
            if (rowMargin < 0)
                throw new Exception("Specified labels will not fit in page height");
            var labelsPerPage = pSettings.Rows * pSettings.Cols;
            
            Document doc = null;
            try
            {
                doc = new Document(new Rectangle(pSettings.Width, pSettings.Height.Value), pSettings.MarginLeft, pSettings.MarginRight, pSettings.MarginTop, pSettings.MarginBottom);
                var writer = PdfWriter.GetInstance(doc, pOutputStream);
                //this should default to "Actual Size"
                writer.AddViewerPreference(PdfName.PRINTSCALING, PdfName.NONE);
                doc.Open();

                var lblIdx = 0;
                var tbl = new PdfPTable(tableCols);
                tbl.WidthPercentage = 100;
                tbl.SetWidths(colWidths.ToArray());
                while (lblIdx < pLabelData.Count)
                {
                    if ((lblIdx % labelsPerPage > 0) && (rowMargin > 0))
                    {
                        //add padding row
                        AddRow(tbl, null, tableCols, rowMargin);
                    }

                    var rowCells = new List<PdfPCell>();
                    for (int colIdx = 0; colIdx <= pSettings.Cols - 1; colIdx++)
                    {
                        object curLabelData = null;
                        if (lblIdx < pLabelData.Count)
                            curLabelData = pLabelData[lblIdx];

                        if (colIdx > 0)
                        {
                            //add padding cell
                            var blankCell = new PdfPCell(new Phrase(" "));
                            blankCell.BorderWidth = 0;
                            rowCells.Add(blankCell);
                        }
                        var cell = pLabelCellRenderer(curLabelData);
                        cell.BorderWidth = this.BorderWidth;
                        rowCells.Add(cell);

                        lblIdx += 1;
                    }
                    AddRow(tbl, rowCells, tableCols, pSettings.LabelHeight);
                }
                doc.Add(tbl);
            }
            finally
            {
                if (doc != null)
                {
                    //prevent the "no pages" error
                    if (doc.PageNumber == 0) doc.Add(new Chunk(' '));
                    doc.Close();
                }
            }
        }


        private void AddRow(PdfPTable pTable, IEnumerable<PdfPCell> pCells, int pColCount, Single pHeight)
        {
            if (pCells == null) pCells = new List<PdfPCell>();
            int added = 0;
            foreach (var cell in pCells)
            {
                cell.FixedHeight = pHeight;
                pTable.AddCell(cell);
                added++;
            }
            while (added < pColCount)
            {
                var cell = new PdfPCell(new Phrase(" "));
                cell.FixedHeight = pHeight;
                cell.BorderWidth = 0;
                pTable.AddCell(cell);
                added++;
            }
        }

    }

}
