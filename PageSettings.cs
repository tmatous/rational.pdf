﻿//  Copyright 2014 Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rational.PDF
{
    public class PageSettings
    {

        public enum ePageSize
        {
            Letter,
            Letter_Landscape
        }

        public enum eMargins
        {
            Normal,
            Narrow
        }


        public PageSettings()
        {
            Init(ePageSize.Letter, eMargins.Normal);
        }

        public PageSettings(ePageSize pPresetSize)
        {
            Init(pPresetSize, eMargins.Normal);
        }

        public PageSettings(ePageSize pPresetSize, eMargins pPresetMargins)
        {
            Init(pPresetSize, pPresetMargins);
        }

        protected void Init(ePageSize pPresetSize, eMargins pPresetMargins)
        {
            switch (pPresetSize)
            {
                case ePageSize.Letter:
                    this.Width = Utilities.InchToPoint(8.5f);
                    this.Height = Utilities.InchToPoint(11f);
                    break;
                case ePageSize.Letter_Landscape:
                    this.Width = Utilities.InchToPoint(11f);
                    this.Height = Utilities.InchToPoint(8.5f);
                    break;
                default:
                    break;
            }

            switch (pPresetMargins)
            {
                case eMargins.Normal:
                    this.MarginLeft = Utilities.InchToPoint(1f);
                    this.MarginRight = Utilities.InchToPoint(1f);
                    this.MarginTop = Utilities.InchToPoint(1f);
                    this.MarginBottom = Utilities.InchToPoint(1f);
                    break;
                case eMargins.Narrow:
                    this.MarginLeft = Utilities.InchToPoint(0.5f);
                    this.MarginRight = Utilities.InchToPoint(0.5f);
                    this.MarginTop = Utilities.InchToPoint(0.5f);
                    this.MarginBottom = Utilities.InchToPoint(0.5f);
                    break;
                default:
                    break;
            }
        }

        public Single Width { get; set; }
        public Single? Height { get; set; }
        public Single MarginRight { get; set; }
        public Single MarginLeft { get; set; }
        public Single MarginTop { get; set; }
        public Single MarginBottom { get; set; }
    }
}
