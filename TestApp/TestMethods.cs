﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Rational.PDF;

namespace TestApp
{
    public class TestMethods : ITestMethods
    {

        public string TestCollectionName { get; set; }
        private static NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();


        public void InitTestEnvironment()
        {
        }

        public void CleanupTestEnvironment()
        {
        }


        private class TestPdfData
        {
            public string Name { get; set; }
            public string Address { get; set; }
        }

        private class TestPdfLoops
        {
            public List<TestPdfData> People { get; set; }
        }



        public void XmlToPdf()
        {
            var generator = new Rational.PDF.XmlToPdf();
            dynamic outputFile = "d:\\temp\\TestPdf.pdf";
            dynamic layoutFile = "TestPdf.xml";
            generator.CreatePdfFile(new System.IO.FileInfo(layoutFile), new System.IO.FileInfo(outputFile), null);

            Process.Start(outputFile);
        }

        public void XmlToPdf2()
        {
            var generator = new Rational.PDF.XmlToPdf2();
            dynamic outputFile = "d:\\temp\\TestPdf.pdf";
            dynamic layoutFile = "TestPdf.xml";
            generator.CreatePdfFile(new System.IO.FileInfo(layoutFile), new System.IO.FileInfo(outputFile), null);

            Process.Start(outputFile);
        }

        public void XmlToPdfFixed()
        {
            var generator = new Rational.PDF.XmlToPdf();
            dynamic outputFile = "d:\\temp\\TestPdfFixed.pdf";
            dynamic layoutFile = "TestPdfFixed.xml";
            generator.CreatePdfFile(new System.IO.FileInfo(layoutFile), new System.IO.FileInfo(outputFile), null);

            Process.Start(outputFile);
        }

        public void XmlToPdfData()
        {
            dynamic outputFile = "d:\\temp\\TestPdfData.pdf";
            dynamic layoutFile = "TestPdfData.xml";
            var data = new TestPdfData
            {
                Name = "Bill Smith",
                Address = "123 Maple St" + Environment.NewLine + "Schenectady, NY  12345"
            };

            var generator = new Rational.PDF.XmlToPdf();
            generator.CreatePdfFile(new System.IO.FileInfo(layoutFile), new System.IO.FileInfo(outputFile), data);

            Process.Start(outputFile);
        }

        public void XmlToPdfLoops()
        {
            dynamic outputFile = "d:\\temp\\TestPdfLoops.pdf";
            dynamic layoutFile = "TestPdfLoops.xml";
            var data = new TestPdfLoops();
            data.People = new List<TestPdfData>();
            data.People.Add(new TestPdfData
            {
                Name = "Bill Smith",
                Address = "123 Maple St" + Environment.NewLine + "Schenectady, NY  12345"
            });
            data.People.Add(new TestPdfData
            {
                Name = "Tom Jones",
                Address = "456 Oak St" + Environment.NewLine + "Beverly Hills, CA  980210"
            });

            var generator = new Rational.PDF.XmlToPdf();
            generator.CreatePdfFile(new System.IO.FileInfo(layoutFile), new System.IO.FileInfo(outputFile), data);

            Process.Start(outputFile);
        }





        public class TestLabelData
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string Barcode { get; set; }
            public DateTime? CreatedDate { get; set; }
        }


        public void LabelsSimple()
        {
            var outputFile = "d:\\temp\\labels.pdf";
            var settings = new Rational.PDF.Labels.LabelPageSettings(PageSettings.ePageSize.Letter);
            settings.MarginRight = Rational.PDF.Utilities.InchToPoint(0.5f);
            settings.Rows = 8;
            settings.Cols = 2;
            settings.LabelHeight = Rational.PDF.Utilities.InchToPoint(0.8f);
            settings.LabelWidth = Rational.PDF.Utilities.InchToPoint(3f);

            List<string> labelData = new List<string>();
            for (int x = 1; x <= 500; x++)
            {
                labelData.Add(string.Format("New label {1}{0}Test text{0}blah", Environment.NewLine, x));
            }

            var labelStyle = new Rational.PDF.Labels.LabelStyle
            {
                FontFamily = "Comic Sans MS",
                FontSize = 8,
                FontColor = System.Drawing.Color.Red,
                HorizontalAlign = eHorizontalAlign.Right,
                VerticalAlign = eVerticalAlign.Middle,
                Rotation = eRotation.Invert
            };

            var lblMaker = new Rational.PDF.Labels.LabelGenerator();
            lblMaker.BorderWidth = 0.5f;
            lblMaker.GenerateLabelsFile(new System.IO.FileInfo(outputFile), settings, labelData, labelStyle);

            Process.Start(outputFile);
        }


        public void LabelsTemplate()
        {
            var outputFile = "d:\\temp\\labels.pdf";
            var settings = new Rational.PDF.Labels.LabelPageSettings(PageSettings.ePageSize.Letter);
            settings.MarginRight = Rational.PDF.Utilities.InchToPoint(0.5f);
            settings.Rows = 8;
            settings.Cols = 2;
            settings.LabelHeight = Rational.PDF.Utilities.InchToPoint(0.8f);
            settings.LabelWidth = Rational.PDF.Utilities.InchToPoint(3f);


            List<TestLabelData> labelData2 = new List<TestLabelData>();
            labelData2.Add(new TestLabelData
            {
                Name = "Error test <blah>",
                Description = "A very long description blah blah test junk asdkfj kasd;fklja ;sdklfjk asdfljkhalksjdfhlkja sdfljkh ljkashdf end",
                Barcode = "",
                CreatedDate = null
            });
            for (int x = 1; x <= 500; x++)
            {
                labelData2.Add(new TestLabelData
                {
                    Name = string.Format("Test Name {0}", x),
                    Description = string.Format("Test description {0} blah blah", x),
                    Barcode = string.Format("*barcode {0}*", x),
                    CreatedDate = DateTime.Today.AddDays(-x)
                });
            }

            var labelTemplate = "<template font-family=\"Arial\" font-size=\"12\" vertical-align=\"middle\" horizontal-align=\"left\" rotation=\"none\" margin-left=\"50\" margin-right=\"50\"><span font-size=\"14\" font-color=\"#3333ff\">{Name}</span><br /><span font-family=\"Comic Sans MS\" font-size=\"8\">{Description}</span><br /><span font-family=\"free39.ttf\" font-size=\"12\">{Barcode}</span><br /><span>{CreatedDate:yyyy-MMM-dd}</span></template>";

            var lblMaker = new Rational.PDF.Labels.LabelGenerator();
            lblMaker.BorderWidth = 0.5f;
            lblMaker.GenerateLabelsFile(new System.IO.FileInfo(outputFile), settings, labelData2, labelTemplate);

            Process.Start(outputFile);
        }


        public class AddressLabelData
        {
            public string Name { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
        }

        public void ReturnAddressLabels_5195()
        {
            var outputFile = "d:\\temp\\addresslabels.pdf";
            var settings = new Rational.PDF.Labels.LabelPageSettings(PageSettings.ePageSize.Letter);
            settings.MarginRight = Rational.PDF.Utilities.InchToPoint(0.25f);
            settings.MarginLeft = Rational.PDF.Utilities.InchToPoint(0.25f);
            settings.MarginTop = Rational.PDF.Utilities.InchToPoint(0.5f);
            settings.MarginBottom = Rational.PDF.Utilities.InchToPoint(0.5f);
            settings.Rows = 15;
            settings.Cols = 4;
            settings.LabelHeight = Rational.PDF.Utilities.InchToPoint(0.6666667f);
            settings.LabelWidth = Rational.PDF.Utilities.InchToPoint(1.75f);

            var labelData = new List<AddressLabelData>();
            for (int x = 1; x <= 60; x++)
            {
                labelData.Add(new AddressLabelData
                {
                    Name = "Joe Smith",
                    Address1 = "800 Maple St",
                    Address2 = "Schenectady, NY 12345"
                });
            }

            var labelTemplate = "<template font-family=\"Arial\" font-size=\"8\" vertical-align=\"middle\" horizontal-align=\"left\" margin-left=\"15\" margin-right=\"5\"><span font-size=\"10\">{Name}</span><br /><span font-size=\"8\">{Address1}</span><br /><span font-size=\"8\">{Address2}</span><br /></template>";

            var lblMaker = new Rational.PDF.Labels.LabelGenerator();
            
            //show border for debugging
            //lblMaker.BorderWidth = 0.5f;

            lblMaker.GenerateLabelsFile(new System.IO.FileInfo(outputFile), settings, labelData, labelTemplate);

            Process.Start(outputFile);
        }


        //error in xml template, so used as a string
        public void LabelsStringTemplate()
        {
            var outputFile = "d:\\temp\\labels.pdf";
            var settings = new Rational.PDF.Labels.LabelPageSettings(PageSettings.ePageSize.Letter);
            settings.MarginRight = Rational.PDF.Utilities.InchToPoint(0.5f);
            settings.Rows = 8;
            settings.Cols = 2;
            settings.LabelHeight = Rational.PDF.Utilities.InchToPoint(0.8f);
            settings.LabelWidth = Rational.PDF.Utilities.InchToPoint(3f);


            List<TestLabelData> labelData2 = new List<TestLabelData>();
            labelData2.Add(new TestLabelData
            {
                Name = "Error test <blah>",
                Description = null,
                Barcode = "",
                CreatedDate = null
            });
            for (int x = 1; x <= 500; x++)
            {
                labelData2.Add(new TestLabelData
                {
                    Name = string.Format("Test Name {0}", x),
                    Description = string.Format("Test description {0} blah blah", x),
                    Barcode = string.Format("*barcode {0}*", x),
                    CreatedDate = DateTime.Today.AddDays(-x)
                });
            }

            var labelTemplate =
@"{Name}
{Description}
{CreatedDate:yyyy-MMM-dd}";

            var lblMaker = new Rational.PDF.Labels.LabelGenerator();
            lblMaker.BorderWidth = 0.5f;
            lblMaker.GenerateLabelsFile(new System.IO.FileInfo(outputFile), settings, labelData2, labelTemplate);

            Process.Start(outputFile);
        }

    }
}
