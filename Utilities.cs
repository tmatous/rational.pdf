﻿//  Copyright 2014 Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Rational.PDF
{
    public class Utilities
    {

        public static Single InchToPoint(Single pInches)
        {
            return pInches * 72;
        }

        private static bool _systemFontsRegistered = false;
        private static List<string> _registeredFonts = null;
        internal static string CheckFontName(string pName)
        {
            if (_registeredFonts == null) _registeredFonts = FontFactory.RegisteredFonts.Cast<string>().ToList();

            var found = false;
            if (_registeredFonts.Contains(pName, StringComparer.OrdinalIgnoreCase))
            {
                found = true;
            }
            else
            {
                //try to register, but not more than once
                if (!_systemFontsRegistered)
                {
                    FontFactory.RegisterDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Fonts));
                    _systemFontsRegistered = true;
                    _registeredFonts = FontFactory.RegisteredFonts.Cast<string>().ToList();
                    if ((_registeredFonts.Contains(pName, StringComparer.OrdinalIgnoreCase)))
                    {
                        found = true;
                    }
                }
            }

            if ((!found))
            {
                //TODO: try to find?
            }

            return pName;
        }


        internal static void SetTextStyle(Chunk pChunk, TextStyle pStyle)
        {
            pChunk.Font = TranslateFont(pStyle);
        }

        internal static void SetParagraphStyle(Paragraph pPara, ParagraphStyle pStyle)
        {
            pPara.Font = TranslateFont(pStyle);
            pPara.Alignment = TranslateHorizontalAlign(pStyle.HorizontalAlign);
            pPara.SpacingBefore = pStyle.MarginTop;
            pPara.SpacingAfter = pStyle.MarginBottom;
            pPara.IndentationLeft = pStyle.MarginLeft;
            pPara.IndentationRight = pStyle.MarginRight;
        }

        internal static Font TranslateFont(TextStyle pStyle)
        {
            int fontStyle = Font.NORMAL;
            switch (pStyle.FontStyle)
            {
                case eFontStyle.Bold:
                    fontStyle = Font.BOLD;
                    break;
                case eFontStyle.Italic:
                    fontStyle = Font.ITALIC;
                    break;
            }

            var fnt = FontFactory.GetFont(Utilities.CheckFontName(pStyle.FontFamily), BaseFont.CP1250, embedded: true, size: pStyle.FontSize, style: fontStyle);
            fnt.Color = new BaseColor(pStyle.FontColor);

            return fnt;
        }

        internal static int TranslateVerticalAlign(eVerticalAlign pAlign)
        {
            int align = Element.ALIGN_TOP;
            switch (pAlign)
            {
                case eVerticalAlign.Middle:
                    align = Element.ALIGN_MIDDLE;
                    break;
                case eVerticalAlign.Bottom:
                    align = Element.ALIGN_BOTTOM;
                    break;
            }

            return align;
        }

        internal static int TranslateHorizontalAlign(eHorizontalAlign pAlign)
        {
            int align = Element.ALIGN_LEFT;
            switch (pAlign)
            {
                case eHorizontalAlign.Center:
                    align = Element.ALIGN_CENTER;
                    break;
                case eHorizontalAlign.Right:
                    align = Element.ALIGN_RIGHT;
                    break;
            }

            return align;
        }

        internal static int TranslateRotation(eRotation pRot)
        {
            int rot = 0;
            switch (pRot)
            {
                case eRotation.Right90:
                    rot = -90;
                    break;
                case eRotation.Left90:
                    rot = 90;
                    break;
                case eRotation.Invert:
                    rot = 180;
                    break;
            }

            return rot;
        }



        internal static object GetObjectProperty(object pObject, string pPropertyName)
        {
            if (pObject == null) return null;

            Int32? idx = null;
            if (pPropertyName.EndsWith("]"))
            {
                var idxPos = pPropertyName.IndexOf('[') + 1;
                idx = Int32.Parse(pPropertyName.Substring(idxPos, pPropertyName.Length - idxPos - 1));
                pPropertyName = pPropertyName.Substring(0, idxPos - 1);
            }
            System.Reflection.PropertyInfo pinfo = pObject.GetType().GetProperty(pPropertyName);
            object res = null;
            if (pinfo != null)
            {
                res = pinfo.GetValue(pObject, null);
            }
            else
            {
                //failsafe for member variables
                System.Reflection.FieldInfo finfo = pObject.GetType().GetField(pPropertyName);
                if (finfo == null) throw new Exception(string.Format("GetObjectProperty: Property or Field '{0}' not found", pPropertyName));
                res = finfo.GetValue(pObject);
            }

            if (idx.HasValue)
            {
                var resEnum = res as IEnumerable<object>;
                if (resEnum == null) throw new Exception("Unable to cast as collection");
                res = resEnum.Skip(idx.Value).Take(1).Single();
            }

            return res;
        }

        internal static object GetObjectPropertyPath(object pObject, string pPropertyPath)
        {
            if (!pPropertyPath.Contains("."))
                return GetObjectProperty(pObject, pPropertyPath);

            string[] subObjectNames = pPropertyPath.Split('.');
            object curObject = pObject;

            for (int i = 0; i <= subObjectNames.Length - 2; i++)
            {
                curObject = GetObjectProperty(curObject, subObjectNames[i]);
            }

            return GetObjectProperty(curObject, subObjectNames[subObjectNames.Length - 1]);
        }
    }
}
