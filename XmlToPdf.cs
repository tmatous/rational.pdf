﻿//  Copyright 2014 Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Rational.PDF
{
    public class XmlToPdf
    {

        private sContextStyle _documentStyle;
        private PageSettings _pageSettings;

        public Func<string, string> MediaResolver { get; set; }


        //this is a struct so it can be safely passed to recursive functions
        private struct sContextStyle
        {
            public Font Font { get; set; }
        }




        public XmlToPdf()
        {
            this.MediaResolver = DefaultMediaResolver;
        }


        public void CreatePdfFile(System.IO.FileInfo pLayoutFile, System.IO.FileInfo pOutputFile)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            CreatePdfFileBase(layout, pOutputFile.FullName, null);
        }

        public void CreatePdfFile(System.IO.FileInfo pLayoutFile, System.IO.FileInfo pOutputFile, object pData)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            CreatePdfFileBase(layout, pOutputFile.FullName, pData);
        }

        public void CreatePdfFile(string pLayout, System.IO.FileInfo pOutputFile, object pData)
        {
            CreatePdfFileBase(pLayout, pOutputFile.FullName, pData);
        }

        public void CreatePdfFile(string pLayout, System.IO.FileInfo pOutputFile)
        {
            CreatePdfFileBase(pLayout, pOutputFile.FullName, null);
        }



        public byte[] CreatePdf(System.IO.FileInfo pLayoutFile)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            return CreatePdfStreamBase(layout, null);
        }

        public byte[] CreatePdf(System.IO.FileInfo pLayoutFile, object pData)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            return CreatePdfStreamBase(layout, pData);
        }

        public byte[] CreatePdf(string pLayout)
        {
            return CreatePdfStreamBase(pLayout, null);
        }

        public byte[] CreatePdf(string pLayout, object pData)
        {
            return CreatePdfStreamBase(pLayout, pData);
        }



        private void CreatePdfFileBase(string pLayout, string pOutputFile, object pData)
        {
            System.IO.FileStream fs = null;
            try
            {
                fs = new System.IO.FileStream(pOutputFile, System.IO.FileMode.Create);
                CreatePdfBase(pLayout, fs, pData);
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        private byte[] CreatePdfStreamBase(string pLayout, object pData)
        {
            var fs = new System.IO.MemoryStream();
            try
            {
                CreatePdfBase(pLayout, fs, pData);
                return fs.ToArray();
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        private void CreatePdfBase(string pLayout, System.IO.Stream pOutputStream, object pData)
        {
            var xmlDoc = LoadLayout(pLayout, pData);

            _pageSettings = new PageSettings
            {
                Width = Utilities.InchToPoint(8.5f),
                Height = Utilities.InchToPoint(11),
                MarginLeft = Utilities.InchToPoint(1),
                MarginRight = Utilities.InchToPoint(1),
                MarginTop = Utilities.InchToPoint(1),
                MarginBottom = Utilities.InchToPoint(1)
            };
            var rootNode = xmlDoc.Root;
            if (rootNode.Name != "pdf") throw new Exception("Invalid root node");

            _pageSettings.Width = GetDimension(rootNode.Attribute("width"), _pageSettings.Width);
            _pageSettings.Height = GetDimension(rootNode.Attribute("height"), _pageSettings.Height.Value);

            _pageSettings.MarginLeft = GetDimension(rootNode.Attribute("margin-left"), _pageSettings.MarginLeft);
            _pageSettings.MarginRight = GetDimension(rootNode.Attribute("margin-right"), _pageSettings.MarginRight);
            _pageSettings.MarginTop = GetDimension(rootNode.Attribute("margin-top"), _pageSettings.MarginTop);
            _pageSettings.MarginBottom = GetDimension(rootNode.Attribute("margin-bottom"), _pageSettings.MarginBottom);

            _documentStyle = GetDefaultStyle();
            _documentStyle.Font = GetFont(rootNode, pDefault: _documentStyle.Font);

            Document doc = null;
            try
            {
                doc = new Document(new Rectangle(_pageSettings.Width, _pageSettings.Height.Value), _pageSettings.MarginLeft, _pageSettings.MarginRight, _pageSettings.MarginTop, _pageSettings.MarginBottom);
                var writer = PdfWriter.GetInstance(doc, pOutputStream);
                //this should default to "Actual Size"
                writer.AddViewerPreference(PdfName.PRINTSCALING, PdfName.NONE);
                doc.Open();

                // loop through the pages
                var pageNodes = rootNode.Elements("page");
                foreach (var pageNode in pageNodes)
                {
                    //If pageNode.Attribute("HeaderText") IsNot Nothing Then
                    //    Dim ht As String = pageNode.Attribute("HeaderText").Value.ToString()
                    //    ht = ResolveVariables(ht)
                    //    Dim hdr As New HeaderFooter(New Phrase(ht, FontFactory.GetFont("Verdana", 8)), False)
                    //    hdr.Border = Rectangle.NO_BORDER
                    //    hdr.Alignment = Element.ALIGN_CENTER
                    //    doc.Header = hdr
                    //Else
                    //    doc.ResetHeader()
                    //End If

                    //If pageNode.Attribute("PageNumbers") IsNot Nothing Then
                    //    Dim ftr As New HeaderFooter(New Phrase("", FontFactory.GetFont("Verdana", 8)), True)
                    //    ftr.Border = Rectangle.NO_BORDER
                    //    ftr.Alignment = Element.ALIGN_RIGHT
                    //    doc.Footer = ftr
                    //Else
                    //    doc.ResetFooter()
                    //End If

                    doc.NewPage();

                    foreach (var childNode in pageNode.Elements())
                    {
                        if (childNode.Name == "fixed")
                        {
                            //fixed position only allowed at root level
                            DrawFixedPosition(childNode, ProcessParagraph(childNode, _documentStyle), writer.DirectContent);
                        }
                        else
                        {
                            doc.Add(ProcessNode(childNode, _documentStyle));
                        }
                    }
                }
            }
            finally
            {
                if (doc != null)
                {
                    //prevent the "no pages" error
                    if (doc.PageNumber == 0) doc.Add(new Chunk(' '));
                    doc.Close();
                }
            }

        }





        private static IElement ProcessNode(XElement pNode, sContextStyle pContextStyle)
        {
            if (pNode.Name == "p")
            {
                return ProcessParagraph(pNode, pContextStyle);
            }
            else if (pNode.Name == "table")
            {
                return ProcessTable(pNode, pContextStyle);
            }
            else if (pNode.Name == "span")
            {
                var spanfnt = GetFont(pNode, pDefault: pContextStyle.Font);
                return new Chunk(TranslateHtmlText(pNode.Value), spanfnt);
            }
            else if (pNode.Name == "br")
            {
                return new Chunk(Environment.NewLine, pContextStyle.Font);
            }
            else if (pNode.Name == "img")
            {
                return ProcessImage(pNode);
            }
            else if (pNode.Name == "hr")
            {
                return ProcessHorizontalRule(pNode);
            }
            else
            {
                throw new Exception(string.Format("Unexpected node: {0}", pNode.Name));
            }
        }

        private static Paragraph ProcessParagraph(XElement pNode, sContextStyle pContextStyle)
        {
            Paragraph result = default(Paragraph);

            pContextStyle.Font = GetFont(pNode, pDefault: pContextStyle.Font);

            result = new Paragraph();
            result.Alignment = GetHorizontalAlign(pNode);
            result.SpacingBefore = GetDimension(pNode.Attribute("margin-top"), 3);
            result.SpacingAfter = GetDimension(pNode.Attribute("margin-bottom"), 3);
            result.IndentationLeft = GetDimension(pNode.Attribute("margin-left"), 0);
            result.IndentationRight = GetDimension(pNode.Attribute("margin-right"), 0);

            foreach (var subNode in pNode.Nodes())
            {
                if (subNode.NodeType == XmlNodeType.Text)
                {
                    result.Add(new Chunk(TranslateHtmlText(((XText)subNode).Value), pContextStyle.Font));
                }
                else if (subNode.NodeType == XmlNodeType.Element)
                {
                    result.Add(ProcessNode((XElement)subNode, pContextStyle));
                }
            }

            return result;
        }

        private static IElement ProcessHorizontalRule(XElement pNode)
        {
            PdfPTable tbl = new PdfPTable(1);
            if (pNode.Attribute("width") != null)
            {
                tbl.TotalWidth = GetDimension(pNode.Attribute("width"), 0);
            }
            else
            {
                tbl.WidthPercentage = 100;
            }
            tbl.SpacingBefore = 10;
            tbl.SpacingAfter = 5;
            PdfPCell c = new PdfPCell(new Paragraph(" "));
            c.BackgroundColor = GetColor(pNode.Attribute("color"), BaseColor.BLACK);
            c.Border = 0;
            c.FixedHeight = GetDimension(pNode.Attribute("height"), 0.5f);
            tbl.AddCell(c);
            return tbl;
        }

        private static IElement ProcessImage(XElement pNode)
        {
            if (pNode.Attribute("src") == null)
                throw new Exception("Image tag requires src attribute");
            var path = pNode.Attribute("src").Value;
            //TODO: should not be static
            path = DefaultMediaResolver(path);
            var img = iTextSharp.text.Image.GetInstance(path);
            img.Alignment = GetHorizontalAlign(pNode);
            //TODO: size

            return img;
        }

        internal static PdfPCell RenderLabelTemplate(string pTemplateXml, object pData)
        {
            var xd = LoadLayout(pTemplateXml, pData);
            var pgh = ProcessParagraph(xd.Root, GetDefaultStyle());
            var cell = new PdfPCell(pgh);
            cell.HorizontalAlignment = GetHorizontalAlign(xd.Root);
            cell.VerticalAlignment = GetVerticalAlign(xd.Root);
            cell.Rotation = GetRotation(xd.Root);
            cell.PaddingLeft = GetDimension(xd.Root.Attribute("margin-left"), 0);
            cell.PaddingRight = GetDimension(xd.Root.Attribute("margin-right"), 0);
            cell.PaddingTop = GetDimension(xd.Root.Attribute("margin-top"), 0);
            cell.PaddingBottom = GetDimension(xd.Root.Attribute("margin-bottom"), 0);

            return cell;
        }

        private static sContextStyle GetDefaultStyle()
        {
            return new sContextStyle
            {
                Font = null
            };
        }

        private static string DefaultMediaResolver(string pOriginalPath)
        {
            if (!System.IO.Path.IsPathRooted(pOriginalPath))
            {
                var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                return System.IO.Path.Combine(path, pOriginalPath);
            }
            return pOriginalPath;
        }

        private static IElement ProcessTable(XElement pNode, sContextStyle pContextStyle)
        {
            int cols = Convert.ToInt32(pNode.Attribute("columns").Value);
            PdfPTable tbl = new PdfPTable(cols);
            if (pNode.Attribute("width") != null)
            {
                tbl.TotalWidth = GetDimension(pNode.Attribute("width"), 0);
            }
            else
            {
                tbl.WidthPercentage = 100;
            }
            float border = 0;
            if (pNode.Attribute("border") != null)
            {
                border = GetDimension(pNode.Attribute("border"),0);
            }
            if (pNode.Attribute("column-widths") != null)
            {
                string colWidthsStr = pNode.Attribute("column-widths").Value;
                int[] colWidths = new int[cols];
                string[] colWidthsStrs = colWidthsStr.Split(',');
                for (int i = 0; i <= cols - 1; i++)
                {
                    if (colWidthsStrs.Length > i)
                    {
                        colWidths[i] = Convert.ToInt32(colWidthsStrs[i]);
                    }
                    else
                    {
                        colWidths[i] = 1;
                    }
                }
                tbl.SetWidths(colWidths);
            }
            tbl.HorizontalAlignment = GetHorizontalAlign(pNode);

            foreach (var rowNode in pNode.Elements())
            {
                if (rowNode.Name != "tr")
                    throw new Exception(string.Format("Unexpected node: {0}", rowNode.Name));
                List<PdfPCell> rowCells = new List<PdfPCell>();
                foreach (var cellNode in rowNode.Elements())
                {
                    if (cellNode.Name != "td")
                        throw new Exception(string.Format("Unexpected node: {0}", rowNode.Name));
                    PdfPCell cell = new PdfPCell();
                    cell.AddElement(ProcessParagraph(cellNode, pContextStyle));
                    cell.BorderWidth = border;
                    rowCells.Add(cell);
                }
                tbl.Rows.Add(new PdfPRow(rowCells.ToArray()));
            }

            return tbl;
        }



        private void DrawFixedPosition(XElement pNode, IElement pElement, PdfContentByte pDirectContent)
        {
            var left = GetDimension(pNode.Attribute("left"), 0);
            var top = GetDimension(pNode.Attribute("top"), 0);

            float width = 0;
            if (pNode.Attribute("width") != null)
            {
                width = GetDimension(pNode.Attribute("width"), 0);
            }
            else
            {
                width = _pageSettings.Width - left;
            }
            if (width <= 0)
                width = 1;

            // 1-column, 1-row table
            PdfPTable tbl = new PdfPTable(1);
            tbl.TotalWidth = width;
            PdfPCell c1 = new PdfPCell();
            c1.Border = 0;
            c1.AddElement(pElement);
            tbl.AddCell(c1);
            //write to specific position
            //coordinates are from bottom-left
            float modTop = _pageSettings.Height.Value - top;
            tbl.WriteSelectedRows(0, -1, left, modTop, pDirectContent);
        }

        private static XDocument LoadLayout(string pTemplateXml, object pData)
        {
            var templateXml = pTemplateXml;
            if (templateXml.Contains("</loop>"))
            {
                templateXml = ExpandTemplateLoops(templateXml, pData);
            }
            if (pData != null)
            {
                templateXml = PopulateTemplateData(templateXml, pData);
            }
            var xd = XDocument.Parse(templateXml);
            return xd;
        }

        internal static bool CheckLayout(string pTemplateXml, object pData)
        {
            try
            {
                var lyt = LoadLayout(pTemplateXml, pData);
                if (lyt != null) return true;
            }
            catch (Exception) { }
            return false;
        }

        private static string ExpandTemplateLoops(string pTemplateXml, object pData)
        {
            if (pData == null)
                throw new Exception("Data is required for a data-bound template");
            var xdoc = XDocument.Parse(pTemplateXml);
            var loopNode = xdoc.Descendants("loop").FirstOrDefault();
            while (loopNode != null)
            {
                var reader = loopNode.CreateReader();
                reader.MoveToContent();
                var loopContent = reader.ReadInnerXml();

                var collectionName = loopNode.Attribute("collection").Value;
                var collection = Utilities.GetObjectPropertyPath(pData, collectionName) as IEnumerable;
                if (collection == null) throw new Exception(string.Format("Collection not found ({0})", collectionName));
                var indexName = loopNode.Attribute("index").Value;
                var curIdx = 0;
                var finalContent = "<loopwrapper>";
                foreach (var item in collection)
                {
                    finalContent += loopContent.Replace(string.Format("[{0}]", indexName), string.Format("[{0}]", curIdx));
                    curIdx++;
                }
                finalContent += "</loopwrapper>";

                var contentTempDoc = XElement.Parse(finalContent);
                loopNode.ReplaceWith(contentTempDoc.Nodes());

                loopNode = xdoc.Descendants("loop").FirstOrDefault();
            }

            return xdoc.ToString();
        }

        internal static string PopulateTemplateData(string pFormat, object pData)
        {
            if (pFormat == null)
                throw new ArgumentNullException("pFormat");

            string result = System.Text.RegularExpressions.Regex.Replace(pFormat, "(?<start>\\{)+(?<property>[\\w\\.\\[\\]]+)(?<format>:[^}]+)?(?<end>\\})+", (System.Text.RegularExpressions.Match m) =>
            {
                var startGroup = m.Groups["start"];
                var propertyGroup = m.Groups["property"];
                var formatGroup = m.Groups["format"];
                var endGroup = m.Groups["end"];

                var openings = startGroup.Captures.Count;
                var closings = endGroup.Captures.Count;
                if ((closings % 2 == 1) && (openings % 2 == 1))
                {
                    var data = Utilities.GetObjectPropertyPath(pData, propertyGroup.Value);
                    if (data == null)
                    {
                        return "";
                    }
                    else if (string.IsNullOrEmpty(formatGroup.Value))
                    {
                        return EscapeXmlText(data.ToString());
                    }
                    else
                    {
                        return EscapeXmlText(string.Format(string.Format("{{0{0}}}", formatGroup.Value), data));
                    }
                }
                else
                {
                    //braces not matched, give up
                    return m.Value;
                }
            }, System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            return result;
        }

        private static string EscapeXmlText(string pSource)
        {
            return pSource.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
        }

        private static string TranslateHtmlText(string pSource)
        {
            //TODO: entity codes?
            return System.Text.RegularExpressions.Regex.Replace(pSource, "\\s+", " ").Trim();
        }

        private static Int32 GetHorizontalAlign(XElement pXmlNode, eHorizontalAlign pDefault = eHorizontalAlign.Left)
        {
            var align = pDefault;
            if (pXmlNode.Attribute("horizontal-align") != null)
            {
                System.Enum.TryParse(pXmlNode.Attribute("horizontal-align").Value, true, out align);
            }
            return Utilities.TranslateHorizontalAlign(align);
        }

        private static Int32 GetVerticalAlign(XElement pXmlNode, eVerticalAlign pDefault = eVerticalAlign.Top)
        {
            var align = pDefault;
            if (pXmlNode.Attribute("vertical-align") != null)
            {
                System.Enum.TryParse(pXmlNode.Attribute("vertical-align").Value, true, out align);
            }
            return Utilities.TranslateVerticalAlign(align);
        }

        private static Int32 GetRotation(XElement pXmlNode, eRotation pDefault = eRotation.None)
        {
            var rot = pDefault;
            if (pXmlNode.Attribute("rotation") != null)
            {
                System.Enum.TryParse(pXmlNode.Attribute("rotation").Value, true, out rot);
            }
            return Utilities.TranslateRotation(rot);
        }

        private static Font GetFont(XElement pXmlNode, bool pEmbedded = true, Font pDefault = null)
        {
            if (pDefault == null)
            {
                pDefault = FontFactory.GetFont(FontFactory.HELVETICA, 12);
            }
            Font result = default(Font);

            if (pXmlNode.Attribute("font-family") != null)
            {
                var fontname = pXmlNode.Attribute("font-family").Value;
                //If (fontname.EndsWith(".ttf")) Then fontname = _windowsFonts + fontname
                result = FontFactory.GetFont(Utilities.CheckFontName(fontname), BaseFont.CP1250, pEmbedded, pDefault.Size, pDefault.Style);
            }
            else
            {
                result = new Font(pDefault);
            }

            result.Size = GetDimension(pXmlNode.Attribute("font-size"), result.Size);

            var fontStyleStr = "";
            if (pXmlNode.Attribute("font-style") != null)
            {
                fontStyleStr = pXmlNode.Attribute("font-style").Value;
            }
            int fontStyle = Font.NORMAL;
            switch (fontStyleStr)
            {
                case "bold":
                    fontStyle = Font.BOLD;
                    break;
                case "italic":
                    fontStyle = Font.ITALIC;
                    break;
            }
            result.SetStyle(fontStyle);

            result.Color = GetColor(pXmlNode.Attribute("font-color"), pDefault.Color);

            return result;
        }

        private static BaseColor GetColor(XAttribute pXmlAttribute, BaseColor pDefault)
        {
            if (pXmlAttribute != null)
            {
                var colorStr = pXmlAttribute.Value;
                var clr = System.Drawing.ColorTranslator.FromHtml(colorStr);
                return new BaseColor(clr);
            }
            else
            {
                return pDefault;
            }
        }

        private static Single GetDimension(XAttribute pXmlAttribute, Single pDefault)
        {
            if (pXmlAttribute != null)
            {
                try
                {
                    var valStr = pXmlAttribute.Value.Trim();
                    if (valStr.EndsWith("in"))
                    {
                        return Utilities.InchToPoint(Convert.ToSingle(valStr.Replace("in", "")));
                    }
                    else if (valStr.EndsWith("pt"))
                    {
                        return Convert.ToSingle(valStr.Replace("pt", ""));
                    }
                    else
                    {
                        return Convert.ToSingle(valStr);
                    }
                }
                catch (Exception)
                {
                    return pDefault;
                }
            }
            else
            {
                return pDefault;
            }
        }


    }
}
