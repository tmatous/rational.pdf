﻿//  Copyright 2014 Tony Matous
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License. 


using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Rational.PDF
{
    public class XmlToPdf2
    {

        private const string DefaultParagraphStyle = "DefaultParagraphStyle";
        private const string DefaultFixedParagraphStyle = "DefaultFixedParagraphStyle";
        private const string DefaultTableCellStyle = "DefaultTableCellStyle";
        private const string DefaultHorizontalRuleStyle = "DefaultHorizontalRuleStyle";

        public string ImageSearchPath { get; set; }

        public XmlToPdf2()
        {
        }


        public void CreatePdfFile(System.IO.FileInfo pLayoutFile, System.IO.FileInfo pOutputFile)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            CreatePdfFileBase(layout, pOutputFile.FullName, null);
        }

        public void CreatePdfFile(System.IO.FileInfo pLayoutFile, System.IO.FileInfo pOutputFile, object pData)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            CreatePdfFileBase(layout, pOutputFile.FullName, pData);
        }

        public void CreatePdfFile(string pLayout, System.IO.FileInfo pOutputFile, object pData)
        {
            CreatePdfFileBase(pLayout, pOutputFile.FullName, pData);
        }

        public void CreatePdfFile(string pLayout, System.IO.FileInfo pOutputFile)
        {
            CreatePdfFileBase(pLayout, pOutputFile.FullName, null);
        }



        public byte[] CreatePdf(System.IO.FileInfo pLayoutFile)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            return CreatePdfStreamBase(layout, null);
        }

        public byte[] CreatePdf(System.IO.FileInfo pLayoutFile, object pData)
        {
            if (!pLayoutFile.Exists) throw new Exception("Layout not found");
            var layout = System.IO.File.ReadAllText(pLayoutFile.FullName);
            return CreatePdfStreamBase(layout, pData);
        }

        public byte[] CreatePdf(string pLayout)
        {
            return CreatePdfStreamBase(pLayout, null);
        }

        public byte[] CreatePdf(string pLayout, object pData)
        {
            return CreatePdfStreamBase(pLayout, pData);
        }



        private void CreatePdfFileBase(string pLayout, string pOutputFile, object pData)
        {
            System.IO.FileStream fs = null;
            try
            {
                fs = new System.IO.FileStream(pOutputFile, System.IO.FileMode.Create);
                CreatePdfBase(pLayout, fs, pData);
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        private byte[] CreatePdfStreamBase(string pLayout, object pData)
        {
            var fs = new System.IO.MemoryStream();
            try
            {
                CreatePdfBase(pLayout, fs, pData);
                return fs.ToArray();
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        private void CreatePdfBase(string pLayout, System.IO.Stream pOutputStream, object pData)
        {
            var xmlDoc = LoadLayout(pLayout, pData);
            var rootNode = xmlDoc.Root;
            if (rootNode.Name != "pdf") throw new Exception("Invalid root node");

            Document doc = null;
            try
            {
                doc = new Document();
                if (!string.IsNullOrWhiteSpace(ImageSearchPath)) doc.ImagePath = ImageSearchPath;

                var normalStyle = doc.Styles[StyleNames.Normal];
                var nsMod = false;
                UpdateFontStyle(rootNode, normalStyle, normalStyle, ref nsMod);

                //set up P style
                var paraStyle = doc.AddStyle(DefaultParagraphStyle, StyleNames.Normal);
                paraStyle.ParagraphFormat.SpaceBefore = Unit.FromPoint(12);
                paraStyle.ParagraphFormat.SpaceAfter = Unit.FromPoint(12);

                //set up fixed para style
                var fixedParaStyle = doc.AddStyle(DefaultFixedParagraphStyle, StyleNames.Normal);
                fixedParaStyle.ParagraphFormat.SpaceBefore = 0;
                fixedParaStyle.ParagraphFormat.SpaceAfter = 0;

                //set up CELL style
                var cellStyle = doc.AddStyle(DefaultTableCellStyle, StyleNames.Normal);
                paraStyle.ParagraphFormat.SpaceBefore = Unit.FromPoint(12);
                paraStyle.ParagraphFormat.SpaceAfter = Unit.FromPoint(12);

                //set up HR style
                var hrStyle = doc.AddStyle(DefaultHorizontalRuleStyle, StyleNames.Normal);
                var hrBorder = new Border();
                hrBorder.Width = 0.5;
                hrBorder.Color = Colors.Black;
                hrStyle.ParagraphFormat.Borders.Bottom = hrBorder;
                hrStyle.ParagraphFormat.LineSpacing = 0;
                hrStyle.ParagraphFormat.SpaceBefore = 10;
                hrStyle.ParagraphFormat.SpaceAfter = 10;
                hrStyle.Font.Size = 1;

                var defaultPage = new PageSetup();
                defaultPage.PageWidth = GetDimension(rootNode.Attribute("width"), Unit.FromInch(11));
                defaultPage.PageHeight = GetDimension(rootNode.Attribute("height"), Unit.FromInch(8.5));
                defaultPage.LeftMargin = GetDimension(rootNode.Attribute("margin-left"), Unit.FromInch(1));
                defaultPage.RightMargin = GetDimension(rootNode.Attribute("margin-right"), Unit.FromInch(1));
                defaultPage.TopMargin = GetDimension(rootNode.Attribute("margin-top"), Unit.FromInch(1));
                defaultPage.BottomMargin = GetDimension(rootNode.Attribute("margin-bottom"), Unit.FromInch(1));

                //this should default to "Actual Size"
                //writer.AddViewerPreference(PdfName.PRINTSCALING, PdfName.NONE);

                // loop through the pages
                var sectionNodes = rootNode.Elements("section");
                foreach (var secNode in sectionNodes)
                {
                    //If pageNode.Attribute("HeaderText") IsNot Nothing Then
                    //    Dim ht As String = pageNode.Attribute("HeaderText").Value.ToString()
                    //    ht = ResolveVariables(ht)
                    //    Dim hdr As New HeaderFooter(New Phrase(ht, FontFactory.GetFont("Verdana", 8)), False)
                    //    hdr.Border = Rectangle.NO_BORDER
                    //    hdr.Alignment = Element.ALIGN_CENTER
                    //    doc.Header = hdr
                    //Else
                    //    doc.ResetHeader()
                    //End If

                    //If pageNode.Attribute("PageNumbers") IsNot Nothing Then
                    //    Dim ftr As New HeaderFooter(New Phrase("", FontFactory.GetFont("Verdana", 8)), True)
                    //    ftr.Border = Rectangle.NO_BORDER
                    //    ftr.Alignment = Element.ALIGN_RIGHT
                    //    doc.Footer = ftr
                    //Else
                    //    doc.ResetFooter()
                    //End If

                    var sec = doc.AddSection();
                    sec.PageSetup.PageWidth = GetDimension(secNode.Attribute("width"), defaultPage.PageWidth);
                    sec.PageSetup.PageHeight = GetDimension(secNode.Attribute("height"), defaultPage.PageHeight);
                    sec.PageSetup.LeftMargin = GetDimension(secNode.Attribute("margin-left"), defaultPage.LeftMargin);
                    sec.PageSetup.RightMargin = GetDimension(secNode.Attribute("margin-right"), defaultPage.RightMargin);
                    sec.PageSetup.TopMargin = GetDimension(secNode.Attribute("margin-top"), defaultPage.TopMargin);
                    sec.PageSetup.BottomMargin = GetDimension(secNode.Attribute("margin-bottom"), defaultPage.BottomMargin);

                    foreach (var childNode in secNode.Elements())
                    {
                        if (childNode.Name == "fixed")
                        {
                            //fixed position only allowed at root level
                            doc.LastSection.Add(CreateFixed(doc, childNode));
                        }
                        else if (childNode.Name == "p")
                        {
                            doc.LastSection.Add(CreateParagraph(doc, childNode, DefaultParagraphStyle));
                        }
                        else if (childNode.Name == "table")
                        {
                            doc.LastSection.Add(CreateTable(doc, childNode, DefaultTableCellStyle));
                        }
                        else if (childNode.Name == "br")
                        {
                            doc.LastSection.AddParagraph("");
                        }
                        else if (childNode.Name == "hr")
                        {
                            doc.LastSection.Add(CreateHorizontalRule(doc, childNode));
                        }
                        else if (childNode.Name == "img")
                        {
                            doc.LastSection.Add(CreateImage(doc, childNode));
                        }
                        else
                        {
                            throw new Exception(string.Format("Unexpected node: {0}", childNode.Name));
                        }
                    }
                }
            }
            finally
            {
                if (doc != null)
                {
                    var renderer = new MigraDoc.Rendering.PdfDocumentRenderer(unicode: true, fontEmbedding: PdfSharp.Pdf.PdfFontEmbedding.Always);
                    renderer.Document = doc;
                    renderer.RenderDocument();

                    renderer.PdfDocument.Save(pOutputStream, closeStream: true);
                }
            }

        }



        private static Paragraph CreateParagraph(Document pDoc, XElement pNode, string pContextStyle)
        {
            pContextStyle = GetParagraphStyle(pDoc, pNode, pContextStyle);

            var result = new Paragraph();
            result.Style = pContextStyle;

            foreach (var subNode in pNode.Nodes())
            {
                if (subNode.NodeType == XmlNodeType.Text)
                {
                    var txt = HandleWhitespace(((XText)subNode).Value);
                    result.AddText(txt);
                }
                else if (subNode.NodeType == XmlNodeType.Element)
                {
                    result.Add(CreateFormattedText(pDoc, (XElement)subNode, pContextStyle));
                }
            }

            return result;
        }

        private static FormattedText CreateFormattedText(Document pDoc, XElement pNode, string pContextStyle)
        {
            pContextStyle = GetParagraphStyle(pDoc, pNode, pContextStyle);

            var result = new FormattedText();
            result.Style = pContextStyle;

            if (pNode.Name == "span")
            {
                foreach (var subNode in pNode.Nodes())
                {
                    if (subNode.NodeType == XmlNodeType.Text)
                    {
                        var txt = HandleWhitespace(((XText)subNode).Value);
                        result.AddText(txt);
                    }
                    else if (subNode.NodeType == XmlNodeType.Element)
                    {
                        result.Add(CreateFormattedText(pDoc, (XElement)subNode, pContextStyle));
                    }
                }
            }
            else if (pNode.Name == "br")
            {
                result.AddLineBreak();
            }
            else if (pNode.Name == "img")
            {
                result.Add(CreateImage(pDoc, pNode));
            }
            else
            {
                throw new Exception(string.Format("Unexpected node: {0}", pNode.Name));
            }

            return result;
        }

        private static Paragraph CreateHorizontalRule(Document pDoc, XElement pNode)
        {
            var style = GetHorizontalRuleStyle(pDoc, pNode, DefaultHorizontalRuleStyle);

            var result = new Paragraph();
            result.Style = style;

            return result;
        }

        private static MigraDoc.DocumentObjectModel.Shapes.Image CreateImage(Document pDoc, XElement pNode)
        {
            if (pNode.Attribute("src") == null)
                throw new Exception("Image tag requires src attribute");
            var path = pNode.Attribute("src").Value;
            var img = new MigraDoc.DocumentObjectModel.Shapes.Image(path);
            img.Left = GetDimension(pNode.Attribute("left"), 0);
            img.Top = GetDimension(pNode.Attribute("top"), 0);
            img.Width = GetDimension(pNode.Attribute("width"), 0);
            img.Height = GetDimension(pNode.Attribute("height"), 0);

            return img;
        }

        private static Table CreateTable(Document pDoc, XElement pNode, string pContextStyle)
        {
            pContextStyle = GetTableStyle(pDoc, pNode, pContextStyle);
            var tbl = new Table();
            tbl.Style = pContextStyle;
            
            tbl.Borders.Width = GetDimension(pNode.Attribute("border-width"), 0);
            
            if (pNode.Attribute("columns") == null) throw new Exception("Missing columns attribute on table");
            int colCount = Convert.ToInt32(pNode.Attribute("columns").Value);
            Unit[] colWidths = null;
            var tableWidth = GetDimension(pNode.Attribute("width"), 0);
            var docWidth = pDoc.LastSection.PageSetup.PageWidth - pDoc.LastSection.PageSetup.LeftMargin - pDoc.LastSection.PageSetup.RightMargin;
            if (pNode.Attribute("column-widths") != null)
            {
                colWidths = new Unit[colCount];
                string colWidthsStr = pNode.Attribute("column-widths").Value;
                string[] colWidthStrs = colWidthsStr.Split(',');
                for (int i = 0; i < colCount; i++)
                {
                    if (colWidthStrs.Length > i)
                    {
                        if (colWidthStrs[i].EndsWith("%"))
                        {
                            var pct = Convert.ToSingle(colWidthStrs[i].Replace("%", ""));
                            if (tableWidth > 0)
                                colWidths[i] = tableWidth * (pct / 100);
                            else
                                colWidths[i] = docWidth * (pct / 100);
                        }
                        else
                        {
                            colWidths[i] = GetDimension(colWidthStrs[i], 1);
                        }
                    }
                    else
                    {
                        colWidths[i] = 1;
                    }
                }
            }

            for (int cc = 0; cc < colCount; cc++)
            {
                var col = tbl.AddColumn();
                if (colWidths != null) col.Width = colWidths[cc];
            }

            foreach (var rowNode in pNode.Elements())
            {
                if (rowNode.Name != "tr")
                    throw new Exception(string.Format("Unexpected node: {0}", rowNode.Name));
                var row = tbl.AddRow();
                if ((rowNode.Attribute("heading") != null) && (rowNode.Attribute("heading").Value == "true"))
                    row.HeadingFormat = true;
                int curCol = 0;
                foreach (var cellNode in rowNode.Elements())
                {
                    if (cellNode.Name != "td")
                        throw new Exception(string.Format("Unexpected node: {0}", rowNode.Name));
                    if (curCol >= colCount)
                        throw new Exception("Invalid number of td elements");
                    var cell = row.Cells[curCol];

                    if (cellNode.Attribute("background-color") != null)
                        cell.Shading.Color = GetColor(cellNode.Attribute("background-color"), Color.Empty);
                    cell.Add(CreateParagraph(pDoc, cellNode, pContextStyle));
                    curCol++;
                }
            }

            return tbl;
        }

        private static MigraDoc.DocumentObjectModel.Shapes.TextFrame CreateFixed(Document pDoc, XElement pNode)
        {
            var style = GetFixedStyle(pDoc, pNode, DefaultFixedParagraphStyle);

            var result = new MigraDoc.DocumentObjectModel.Shapes.TextFrame();
            result.Left = MigraDoc.DocumentObjectModel.Shapes.ShapePosition.Left;
            result.WrapFormat.DistanceLeft = GetDimension(pNode.Attribute("left"), 0);
            result.Top = MigraDoc.DocumentObjectModel.Shapes.ShapePosition.Top;
            result.WrapFormat.DistanceTop = GetDimension(pNode.Attribute("top"), 0);
            result.Height = GetDimension(pNode.Attribute("height"), 0);
            result.Width = GetDimension(pNode.Attribute("width"), 0);
            result.RelativeHorizontal = MigraDoc.DocumentObjectModel.Shapes.RelativeHorizontal.Page;
            result.RelativeVertical = MigraDoc.DocumentObjectModel.Shapes.RelativeVertical.Page;

            result.LineFormat.Width = GetDimension(pNode.Attribute("border-width"), 0);

            foreach (var childNode in pNode.Elements())
            {
                if (childNode.Name == "p")
                {
                    result.Add(CreateParagraph(pDoc, childNode, style));
                }
                else if (childNode.Name == "table")
                {
                    result.Add(CreateTable(pDoc, childNode, style));
                }
                else if (childNode.Name == "br")
                {
                    result.AddParagraph("");
                }
                else if (childNode.Name == "hr")
                {
                    result.Add(CreateHorizontalRule(pDoc, childNode));
                }
                else if (childNode.Name == "img")
                {
                    result.Add(CreateImage(pDoc, childNode));
                }
                else
                {
                    throw new Exception(string.Format("Unexpected node: {0}", childNode.Name));
                }
            }

            return result;
        }

        private static XDocument LoadLayout(string pTemplateXml, object pData)
        {
            var templateXml = pTemplateXml;
            if (templateXml.Contains("</loop>"))
            {
                templateXml = ExpandTemplateLoops(templateXml, pData);
            }
            if (pData != null)
            {
                templateXml = PopulateTemplateData(templateXml, pData);
            }
            var xd = XDocument.Parse(templateXml);
            return xd;
        }

        internal static bool CheckLayout(string pTemplateXml, object pData)
        {
            try
            {
                var lyt = LoadLayout(pTemplateXml, pData);
                if (lyt != null) return true;
            }
            catch (Exception) { }
            return false;
        }

        private static string ExpandTemplateLoops(string pTemplateXml, object pData)
        {
            if (pData == null)
                throw new Exception("Data is required for a data-bound template");
            var xdoc = XDocument.Parse(pTemplateXml);
            var loopNode = xdoc.Descendants("loop").FirstOrDefault();
            while (loopNode != null)
            {
                var reader = loopNode.CreateReader();
                reader.MoveToContent();
                var loopContent = reader.ReadInnerXml();

                var collectionName = loopNode.Attribute("collection").Value;
                var collection = Utilities.GetObjectPropertyPath(pData, collectionName) as IEnumerable;
                if (collection == null) throw new Exception(string.Format("Collection not found ({0})", collectionName));
                var indexName = loopNode.Attribute("index").Value;
                var curIdx = 0;
                var finalContent = "<loopwrapper>";
                foreach (var item in collection)
                {
                    finalContent += loopContent.Replace(string.Format("[{0}]", indexName), string.Format("[{0}]", curIdx));
                    curIdx++;
                }
                finalContent += "</loopwrapper>";

                var contentTempDoc = XElement.Parse(finalContent);
                loopNode.ReplaceWith(contentTempDoc.Nodes());

                loopNode = xdoc.Descendants("loop").FirstOrDefault();
            }

            return xdoc.ToString();
        }

        internal static string PopulateTemplateData(string pFormat, object pData)
        {
            if (pFormat == null)
                throw new ArgumentNullException("pFormat");

            string result = System.Text.RegularExpressions.Regex.Replace(pFormat, "(?<start>\\{)+(?<property>[\\w\\.\\[\\]]+)(?<format>:[^}]+)?(?<end>\\})+", (System.Text.RegularExpressions.Match m) =>
            {
                var startGroup = m.Groups["start"];
                var propertyGroup = m.Groups["property"];
                var formatGroup = m.Groups["format"];
                var endGroup = m.Groups["end"];

                var openings = startGroup.Captures.Count;
                var closings = endGroup.Captures.Count;
                if ((closings % 2 == 1) && (openings % 2 == 1))
                {
                    var data = Utilities.GetObjectPropertyPath(pData, propertyGroup.Value);
                    if (data == null)
                    {
                        return "";
                    }
                    else if (string.IsNullOrEmpty(formatGroup.Value))
                    {
                        return EscapeXmlText(data.ToString());
                    }
                    else
                    {
                        return EscapeXmlText(string.Format(string.Format("{{0{0}}}", formatGroup.Value), data));
                    }
                }
                else
                {
                    //braces not matched, give up
                    return m.Value;
                }
            }, System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            return result;
        }

        private static string EscapeXmlText(string pSource)
        {
            return pSource.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
        }

        private static string HandleWhitespace(string pSource)
        {
            //TODO: entity codes?
            return System.Text.RegularExpressions.Regex.Replace(pSource, "\\s+", " ");
        }

        private static ParagraphAlignment GetHorizontalAlign(XElement pXmlNode, eHorizontalAlign pDefault = eHorizontalAlign.Left)
        {
            var align = pDefault;
            if (pXmlNode.Attribute("horizontal-align") != null)
            {
                System.Enum.TryParse(pXmlNode.Attribute("horizontal-align").Value, true, out align);
            }
            return TranslateHorizontalAlign2(align);
        }

        internal static ParagraphAlignment TranslateHorizontalAlign2(eHorizontalAlign pAlign)
        {
            var align = ParagraphAlignment.Left;
            switch (pAlign)
            {
                case eHorizontalAlign.Center:
                    align = ParagraphAlignment.Center;
                    break;
                case eHorizontalAlign.Right:
                    align = ParagraphAlignment.Right;
                    break;
            }

            return align;
        }

        internal static VerticalAlignment TranslateVerticalAlign2(eVerticalAlign pAlign)
        {
            var align = VerticalAlignment.Top;
            switch (pAlign)
            {
                case eVerticalAlign.Middle:
                    align = VerticalAlignment.Center;
                    break;
                case eVerticalAlign.Bottom:
                    align = VerticalAlignment.Bottom;
                    break;
            }

            return align;
        }

        private static VerticalAlignment GetVerticalAlign(XElement pXmlNode, eVerticalAlign pDefault = eVerticalAlign.Top)
        {
            var align = pDefault;
            if (pXmlNode.Attribute("vertical-align") != null)
            {
                System.Enum.TryParse(pXmlNode.Attribute("vertical-align").Value, true, out align);
            }
            return TranslateVerticalAlign2(align);
        }

        private static Int32 GetRotation(XElement pXmlNode, eRotation pDefault = eRotation.None)
        {
            var rot = pDefault;
            if (pXmlNode.Attribute("rotation") != null)
            {
                System.Enum.TryParse(pXmlNode.Attribute("rotation").Value, true, out rot);
            }
            return Utilities.TranslateRotation(rot);
        }

        private static string GetParagraphStyle(Document doc, XElement pXmlNode, string pDefaultStyle)
        {
            return GetStyleImpl(doc, pXmlNode, pDefaultStyle, "Paragraph");
        }

        private static string GetTableStyle(Document doc, XElement pXmlNode, string pDefaultStyle)
        {
            return GetStyleImpl(doc, pXmlNode, pDefaultStyle, "Table");
        }

        private static string GetHorizontalRuleStyle(Document doc, XElement pXmlNode, string pDefaultStyle)
        {
            return GetStyleImpl(doc, pXmlNode, pDefaultStyle, "HorizontalRule");
        }

        private static string GetFixedStyle(Document doc, XElement pXmlNode, string pDefaultStyle)
        {
            return GetStyleImpl(doc, pXmlNode, pDefaultStyle, "Fixed");
        }

        private static string GetStyleImpl(Document doc, XElement pXmlNode, string pDefaultStyle, string pType)
        {
            if (string.IsNullOrEmpty(pDefaultStyle)) pDefaultStyle = StyleNames.Normal;
            if (pXmlNode.Attribute("class") != null) pDefaultStyle = pXmlNode.Attribute("class").Value;

            var curStyle = doc.Styles[pDefaultStyle];
            var newStyle = curStyle.Clone();
            var modified = false;

            if (pType == "Paragraph")
            {
                UpdateFontStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateAlignStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateMarginStyle(pXmlNode, curStyle, newStyle, ref modified);
            }
            else if (pType == "Table")
            {
                UpdateAlignStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateMarginStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateTableStyle(pXmlNode, curStyle, newStyle, ref modified);
            }
            else if (pType == "Fixed")
            {
                UpdateFontStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateAlignStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateMarginStyle(pXmlNode, curStyle, newStyle, ref modified);
            }
            else if (pType == "HorizontalRule")
            {
                UpdateAlignStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateMarginStyle(pXmlNode, curStyle, newStyle, ref modified);
                UpdateHrStyle(pXmlNode, curStyle, newStyle, ref modified);
            }
            else throw new Exception("Invalid style type");

            var styleName = pDefaultStyle;
            if (modified)
            {
                //TODO: check for exact match style?
                styleName = Guid.NewGuid().ToString();
                var newStyleInst = doc.Styles.AddStyle(styleName, pDefaultStyle);
                newStyleInst.ParagraphFormat = newStyle.ParagraphFormat.Clone();
            }

            return styleName;
        }

        private static void UpdateFontStyle(XElement pXmlNode, Style pOrigStyle, Style pNewStyle, ref bool pioModified)
        {
            if (pXmlNode.Attribute("font-family") != null)
            {
                pioModified = true;
                pNewStyle.Font.Name = pXmlNode.Attribute("font-family").Value;
                //if (fontname.EndsWith(".ttf")) fontname = _windowsFonts + fontname
            }

            if (pXmlNode.Attribute("font-size") != null)
            {
                pioModified = true;
                pNewStyle.Font.Size = GetDimension(pXmlNode.Attribute("font-size"), pOrigStyle.Font.Size);
            }

            if (pXmlNode.Attribute("font-style") != null)
            {
                pioModified = true;
                var fontStyleStr = pXmlNode.Attribute("font-style").Value;
                switch (fontStyleStr)
                {
                    case "bold":
                        pNewStyle.Font.Bold = true;
                        break;
                    case "italic":
                        pNewStyle.Font.Italic = true;
                        break;
                }
            }

            if (pXmlNode.Attribute("font-color") != null)
            {
                pioModified = true;
                pNewStyle.Font.Color = GetColor(pXmlNode.Attribute("font-color"), pOrigStyle.Font.Color);
            }

        }

        private static void UpdateMarginStyle(XElement pXmlNode, Style pOrigStyle, Style pNewStyle, ref bool pioModified)
        {
            if (pXmlNode.Attribute("margin-top") != null)
            {
                pioModified = true;
                pNewStyle.ParagraphFormat.SpaceBefore = GetDimension(pXmlNode.Attribute("margin-top"), pOrigStyle.ParagraphFormat.SpaceBefore);
            }

            if (pXmlNode.Attribute("margin-bottom") != null)
            {
                pioModified = true;
                pNewStyle.ParagraphFormat.SpaceAfter = GetDimension(pXmlNode.Attribute("margin-bottom"), pOrigStyle.ParagraphFormat.SpaceAfter);
            }

            if (pXmlNode.Attribute("margin-left") != null)
            {
                pioModified = true;
                pNewStyle.ParagraphFormat.LeftIndent = GetDimension(pXmlNode.Attribute("margin-left"), pOrigStyle.ParagraphFormat.LeftIndent);
            }

            if (pXmlNode.Attribute("margin-right") != null)
            {
                pioModified = true;
                pNewStyle.ParagraphFormat.RightIndent = GetDimension(pXmlNode.Attribute("margin-right"), pOrigStyle.ParagraphFormat.RightIndent);
            }
        }

        private static void UpdateAlignStyle(XElement pXmlNode, Style pOrigStyle, Style pNewStyle, ref bool pioModified)
        {
            if (pXmlNode.Attribute("horizontal-align") != null)
            {
                pioModified = true;
                pNewStyle.ParagraphFormat.Alignment = GetHorizontalAlign(pXmlNode);
            }
        }

        private static void UpdateTableStyle(XElement pXmlNode, Style pOrigStyle, Style pNewStyle, ref bool pioModified)
        {
            //none
        }

        private static void UpdateHrStyle(XElement pXmlNode, Style pOrigStyle, Style pNewStyle, ref bool pioModified)
        {
            if ((pXmlNode.Attribute("height") != null) || (pXmlNode.Attribute("color") != null))
            {
                pioModified = true;
                var hrBorder = new Border();
                hrBorder.Width = GetDimension(pXmlNode.Attribute("height"), 0.5);
                hrBorder.Color = GetColor(pXmlNode.Attribute("color"), Colors.Black);
                pNewStyle.ParagraphFormat.Borders.Bottom = hrBorder;
            }
        }

        private static Color GetColor(XAttribute pXmlAttribute, Color pDefault)
        {
            if (pXmlAttribute != null)
            {
                var colorStr = pXmlAttribute.Value;
                var clr = System.Drawing.ColorTranslator.FromHtml(colorStr);
                return new Color(clr.A, clr.R, clr.G, clr.B);
            }
            else
            {
                return pDefault;
            }
        }

        private static Unit GetDimension(XAttribute pXmlAttribute, Unit pDefault)
        {
            var dimStr = "";
            if (pXmlAttribute != null) dimStr = pXmlAttribute.Value;
            return GetDimension(dimStr, pDefault);
        }

        private static Unit GetDimension(string pDimStr, Unit pDefault)
        {
            if (!string.IsNullOrWhiteSpace(pDimStr))
            {
                try
                {
                    pDimStr = pDimStr.Trim();
                    if (pDimStr.EndsWith("in"))
                        return Unit.FromInch(Convert.ToSingle(pDimStr.Replace("in", "")));
                    else if (pDimStr.EndsWith("cm"))
                        return Unit.FromCentimeter(Convert.ToSingle(pDimStr.Replace("cm", "")));
                    else if (pDimStr.EndsWith("mm"))
                        return Unit.FromMillimeter(Convert.ToSingle(pDimStr.Replace("mm", "")));
                    else if (pDimStr.EndsWith("pt"))
                        return Unit.FromPoint(Convert.ToSingle(pDimStr.Replace("pt", "")));
                    else if (pDimStr.EndsWith("px"))
                        return Unit.FromPoint(Convert.ToSingle(pDimStr.Replace("px", "")));
                    else
                        return Unit.FromPoint(Convert.ToSingle(pDimStr));
                }
                catch (Exception)
                {
                    return pDefault;
                }
            }
            else
            {
                return pDefault;
            }
        }


    }
}
