@echo off
set config=Debug
set destdir=.\Build\
echo Ready to copy the %config% assemblies to the %destdir% directory...

set msbuild_exe="C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe"
%msbuild_exe% /t:Build /p:Configuration=%config% Rational.PDF.sln
if ERRORLEVEL 1 goto Error

md %destdir%
copy bin\%config%\*.dll %destdir%
copy bin\%config%\*.xml %destdir%

echo Done.
pause

goto End
:Error
echo Error!
pause
:End
